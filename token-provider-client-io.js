var o2 = require('origami2');
var Crane = o2.Crane;
var RSASocket = o2.RSASocket;
var TokenProviderClientFactory = o2.TokenProviderClientFactory;
var NameRegistry = o2.NameRegistry;

function TokenProviderClientIO(privateKey) {
  this.privateKey = privateKey;
  this.nameRegistry = new NameRegistry(privateKey);
}

TokenProviderClientIO.prototype.connect = function (url, token, notifications) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var socket = require('socket.io-client')(url);
    
    socket
    .on(
      'connect',
      function (socket) {
        self
        .listenSocket(socket, token, notifications)
        .then(resolve)
        .catch(reject);
      }
    );
  });
};

TokenProviderClientIO.prototype.createFactorySocket = function (url) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var socket = require('socket.io-client')(url);
    
    socket
    .on(
      'connect',
      function () {
        self
        .createFactorySocket(socket)
        .then(function (factory) {
          resolve(factory);
        })
        .catch(reject);
      }
    );
  });
};

TokenProviderClientIO.prototype.createFactorySocket = function (socket) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var rsaSocket = new RSASocket(self.privateKey);
      
    rsaSocket
    .connect(socket)
    .then(function (secureSocket) {
      (new Crane(
        self.nameRegistry,
        function () {
          // no incoming connections
          return Promise.reject();
        },
        secureSocket
      ))
      .createSocket(
        'Client',
        function (socket, params) {
          socket
          .emit(
            'describe-token-providers', 
            function (err, apis) {
              if (err) return reject(err);
              
              var factory = new TokenProviderClientFactory(
                socket, 
                self.privateKey, 
                self.nameRegistry, 
                apis
              );
              
              resolve(factory);
            }
          );
          
          return Promise.resolve();
        }
      )
      .catch(reject);
    });
  });
};

TokenProviderClientIO.prototype.listenSocket = function (socket, token, notifications) {
  var self = this;
  
  return new Promise(function (resolve, reject) {
    try {
      self
      .createFactorySocket(socket)
      .then(function (factory) {
        var client = factory(token, notifications);
        
        resolve(client);  
      })
      .catch(reject);
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = TokenProviderClientIO;